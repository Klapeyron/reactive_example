// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <chrono>
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/float64.hpp>
#include <std_msgs/msg/bool.hpp>

using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses a fancy C++11 lambda
 * function to shorten the callback syntax, at the expense of making the
 * code somewhat more difficult to understand at first glance. */

class MinimalPublisher : public rclcpp::Node
{
public:
  MinimalPublisher()
  : Node("minimal_publisher")
  {
    publisher1 = this->create_publisher<std_msgs::msg::Float64>("a1");
    publisher2 = this->create_publisher<std_msgs::msg::Float64>("a2");
    publisher3 = this->create_publisher<std_msgs::msg::Float64>("theta1");
    publisher4 = this->create_publisher<std_msgs::msg::Float64>("theta2");
    publisher5 = this->create_publisher<std_msgs::msg::Bool>("operator_heartbeat");

    { auto message = std_msgs::msg::Float64(); message.data = 2.0; publisher1->publish(message); }
    { auto message = std_msgs::msg::Float64(); message.data = 3.0; publisher2->publish(message); }
    std::this_thread::sleep_for(std::chrono::seconds(1));
    
    { auto message = std_msgs::msg::Float64(); message.data = 0.785398; publisher3->publish(message); }
    { auto message = std_msgs::msg::Float64(); message.data = 0.785398; publisher4->publish(message); }

    for(std::size_t i = 0; i < 10; ++i)
    {
      { auto message = std_msgs::msg::Bool(); message.data = true; publisher5->publish(message); }
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));

    { auto message = std_msgs::msg::Float64(); message.data = 1.91986; publisher3->publish(message); }
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    { auto message = std_msgs::msg::Float64(); message.data = 1.785398; publisher4->publish(message); }
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    { auto message = std_msgs::msg::Float64(); message.data = 1.785398; publisher4->publish(message); }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    { auto message = std_msgs::msg::Float64(); message.data = 1.785398; publisher4->publish(message); }
    std::this_thread::sleep_for(std::chrono::milliseconds(200));

    { auto message = std_msgs::msg::Float64(); message.data = 1.5708; publisher3->publish(message); }
    { auto message = std_msgs::msg::Bool(); message.data = true; publisher5->publish(message); }
    std::this_thread::sleep_for(std::chrono::seconds(2));

    { auto message = std_msgs::msg::Float64(); message.data = 2.35619; publisher3->publish(message); }
  }

private:
  rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr publisher1;
  rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr publisher2;
  rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr publisher3;
  rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr publisher4;
  rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr publisher5;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MinimalPublisher>());
  rclcpp::shutdown();
  return 0;
}
