#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/bool.hpp>
#include <std_msgs/msg/float64.hpp>
#include <std_msgs/msg/string.hpp>

#include <rclcpp/executors/multi_threaded_executor.hpp>
#include <ros2-reactive/ReactiveNode.hpp>

#include <iostream>

int main(int argc, char* argv[])
{
  rclcpp::init(argc, argv);

  rclcpp::executors::MultiThreadedExecutor executor(
      rclcpp::executor::create_default_executor_arguments());

  auto node = std::make_shared<ros2_reactive::ReactiveNode>("Two_link_manipulator");

  auto a1 = node->create_observable<std_msgs::msg::Float64>("a1");
  auto a2 = node->create_observable<std_msgs::msg::Float64>("a2");
  auto theta1 = node->create_observable<std_msgs::msg::Float64>("theta1");
  auto theta2 = node->create_observable<std_msgs::msg::Float64>("theta2");

  auto x_y =
      a1.combine_latest(a2, theta1, theta2)
        .map([](std::tuple<std_msgs::msg::Float64,
                           std_msgs::msg::Float64,
                           std_msgs::msg::Float64,
                           std_msgs::msg::Float64> const& latest_values)
          {
            std_msgs::msg::Float64 a1, a2, theta1, theta2;
            std::tie(a1, a2, theta1, theta2) = latest_values;

            std::cout << "### Received new data: {" << a1.data << ", "
                      << a2.data << ", " << theta1.data << ", " << theta2.data
                      << "}" << std::endl;

            double x = a1.data * std::cos(theta1.data) +
                       a2.data * std::cos(theta1.data + theta2.data);
            double y = a1.data * std::sin(theta1.data) +
                       a2.data * std::sin(theta1.data + theta2.data);
            return std::make_tuple(x, y);
          })
          .publish()
          .ref_count();

  using time_point = rxcpp::schedulers::scheduler::clock_type::time_point;

  auto speed =
      x_y.timestamp()
         .pairwise()
         .map([](auto const& previous_and_current_positions_with_time)
         {
           std::pair<std::tuple<double, double>, time_point> previous;
           std::pair<std::tuple<double, double>, time_point> current;
           std::tie(previous, current) = previous_and_current_positions_with_time;

           double x_diff = std::get<0>(current.first) - std::get<0>(previous.first);
           double y_diff = std::get<1>(current.first) - std::get<1>(previous.first);

           double distance = std::sqrt(std::pow(x_diff, 2) + std::pow(y_diff, 2));
           double time = std::chrono::duration_cast<std::chrono::milliseconds>(
               current.second - previous.second).count() / 1000.0;

           return distance / time;
         })
         .distinct_until_changed();

  node->create_observable<std_msgs::msg::Bool>("operator_heartbeat")
      .tap([](std_msgs::msg::Bool const&) {
        std::cout << "### Received signal from operator" << std::endl;
      })
      .sample_with_time(rxcpp::synchronize_new_thread(),
                        std::chrono::milliseconds(500))
      .timeout(rxcpp::synchronize_new_thread(), std::chrono::seconds(3))
      .tap(
          [](std_msgs::msg::Bool const&) {
            std::cout << "Operator is alive" << std::endl;
          },
          [](std::exception_ptr) {
            std::cout << "Not received signal from operator..." << std::endl;
          })
      .ignore_elements()
      .retry(3)
      .subscribe(
          [](std_msgs::msg::Bool const&) {
            std::cout << "It will be never called !!!" << std::endl;
          },
          [&](std::exception_ptr) {
            std::cout << "Operator is sleeping !!!" << std::endl;
            std::cout << "Turning off engines..." << std::endl;

            auto engines =
                node->create_publisher<std_msgs::msg::String>("engines");
            std_msgs::msg::String emergency_message;
            emergency_message.data = "turn_off";

            engines->publish(emergency_message);
          });

  x_y.subscribe([](std::tuple<double, double> const& tuple) {
    double x, y;
    std::tie(x, y) = tuple;

    std::cout << "Position x: " << x << " and y: " << y << std::endl;
  });

  speed.subscribe(
      [](double const& speed) {
        std::cout << "Speed: " << speed << " [m/s]" << std::endl;
      },
      [](auto) {
        std::cout << "Speed calculation error !!!" << std::endl;
      },
      []() {
        std::cout << "Speed calculation completed" << std::endl;
      });

  executor.add_node(node);
  executor.spin();

  // rclcpp::spin(node);
  rclcpp::shutdown();
}
